create database companieit;
use companieit;

create table angajat (
calificare varchar(20),
cnp char(13) not null primary key,
adresa varchar(200),
salariu float,
telefon int
);

create table proiecte (
nr_proiect int auto_increment primary key,
nume_proiect varchar(200),
buget float
);

create table angajat_proiect (
cnp char(13),
nr_proiect int,
foreign key (cnp) references angajat(cnp),
foreign key (nr_proiect) references proiecte(nr_proiect)
);

describe angajat;

-- tinyint - boolean true/fals - 0 -> fals , 1 -> adevarat
alter table angajat
add column este_manager tinyint default 0;

drop trigger validare_cnp;

delimiter $$
create trigger validare_cnp 
before insert on angajat
for each row 
begin
declare cnp char(13);
declare data_nasterii_txt char(6);
declare data_nasterii date;
declare sex char(1);
declare msg varchar(200);
set cnp = new.cnp;
set data_nasterii_txt = substring(cnp,2,6);
set data_nasterii = str_to_date(data_nasterii_txt,'%y%m%d');
set sex = substring(cnp,1,1);
if sex <> 1 and sex <> 2 and sex <> 5 and sex <> 6 then
set msg = concat('Caracterul ', sex, ' nu reprezinta un cod valid pentru sex');
signal sqlstate '45000' set message_text = msg;
end if;
end $$

DELIMITER ;

describe angajat;

select * from angajat;

insert into angajat
values
-- ('tester','1841210002415','cluj',4500.00,0745216525,0),
-- ('Programator Java','1821009002415','cluj',4500.00,0745226525,1);
-- ('Programator Java','2820228002115','Bucuresti',4800.00,0743126525,0);
 -- ('Programator PHP','1850315000915','Bucuresti',5200.00,0745176595,0);
 ('Programator PHP','1870915020915','Bucuresti',5500.00,0745322595,1);

alter table angajat
add column nume varchar(30) first;

alter table angajat
add column prenume varchar(30) after nume;

alter table angajat
drop column prenume;

-- update angajat nume set nume = 'Anton Popescu' where cnp = '1821009002415';
update angajat nume set nume = 'Liviu Andrei' where cnp = '1841210002415';
update angajat nume set nume = 'Marcel Popescu' where cnp = '1850315000915';
update angajat nume set nume = 'Iulian Ionescu' where cnp = '1870915020915';
update angajat nume set nume = 'Andreea Ion' where cnp = '2820228002115';

